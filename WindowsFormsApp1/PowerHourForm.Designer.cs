﻿namespace WindowsFormsApp1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.eventTimer = new System.Windows.Forms.Timer(this.components);
            this.startButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.timerDisplay = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timerWorker = new System.ComponentModel.BackgroundWorker();
            this.totalTimeBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.eventTimeBox = new System.Windows.Forms.TextBox();
            this.eventTimerLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.actionBox = new System.Windows.Forms.TextBox();
            this.randomizeCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(918, 751);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(145, 53);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.Location = new System.Drawing.Point(1077, 751);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(135, 53);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // timerDisplay
            // 
            this.timerDisplay.AutoSize = true;
            this.timerDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timerDisplay.Location = new System.Drawing.Point(743, 81);
            this.timerDisplay.Name = "timerDisplay";
            this.timerDisplay.Size = new System.Drawing.Size(347, 91);
            this.timerDisplay.TabIndex = 3;
            this.timerDisplay.Text = "00:00:00";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(32, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(478, 163);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // timerWorker
            // 
            this.timerWorker.WorkerSupportsCancellation = true;
            this.timerWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.timerWorker_DoWork);
            // 
            // totalTimeBox
            // 
            this.totalTimeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTimeBox.Location = new System.Drawing.Point(359, 727);
            this.totalTimeBox.Name = "totalTimeBox";
            this.totalTimeBox.Size = new System.Drawing.Size(130, 34);
            this.totalTimeBox.TabIndex = 5;
            this.totalTimeBox.Text = "01:00:00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 727);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Total Time (hh:mm:ss)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 775);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(317, 29);
            this.label2.TabIndex = 7;
            this.label2.Text = "Event Frequency (hh:mm:ss)";
            // 
            // eventTimeBox
            // 
            this.eventTimeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventTimeBox.Location = new System.Drawing.Point(360, 772);
            this.eventTimeBox.Name = "eventTimeBox";
            this.eventTimeBox.Size = new System.Drawing.Size(130, 34);
            this.eventTimeBox.TabIndex = 8;
            this.eventTimeBox.Text = "00:01:00";
            // 
            // eventTimerLabel
            // 
            this.eventTimerLabel.AutoSize = true;
            this.eventTimerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventTimerLabel.Location = new System.Drawing.Point(526, 727);
            this.eventTimerLabel.Name = "eventTimerLabel";
            this.eventTimerLabel.Size = new System.Drawing.Size(347, 91);
            this.eventTimerLabel.TabIndex = 9;
            this.eventTimerLabel.Text = "00:00:00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(738, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(363, 46);
            this.label4.TabIndex = 10;
            this.label4.Text = "Total Time Elapsed";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(521, 681);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(355, 46);
            this.label5.TabIndex = 11;
            this.label5.Text = "Countdown to next";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // actionBox
            // 
            this.actionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actionBox.Location = new System.Drawing.Point(32, 255);
            this.actionBox.Multiline = true;
            this.actionBox.Name = "actionBox";
            this.actionBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.actionBox.Size = new System.Drawing.Size(1180, 411);
            this.actionBox.TabIndex = 12;
            // 
            // randomizeCheckBox
            // 
            this.randomizeCheckBox.AutoSize = true;
            this.randomizeCheckBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.randomizeCheckBox.Checked = true;
            this.randomizeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.randomizeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.randomizeCheckBox.Location = new System.Drawing.Point(1055, 702);
            this.randomizeCheckBox.Name = "randomizeCheckBox";
            this.randomizeCheckBox.Size = new System.Drawing.Size(157, 33);
            this.randomizeCheckBox.TabIndex = 13;
            this.randomizeCheckBox.Text = "Randomize";
            this.randomizeCheckBox.UseVisualStyleBackColor = true;
            this.randomizeCheckBox.CheckedChanged += new System.EventHandler(this.randomizeCheckBox_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 827);
            this.Controls.Add(this.randomizeCheckBox);
            this.Controls.Add(this.actionBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.eventTimerLabel);
            this.Controls.Add(this.eventTimeBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totalTimeBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.timerDisplay);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.startButton);
            this.Name = "MainForm";
            this.Text = "PowerHour";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer eventTimer;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.Label timerDisplay;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.ComponentModel.BackgroundWorker timerWorker;
        private System.Windows.Forms.TextBox totalTimeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox eventTimeBox;
        private System.Windows.Forms.Label eventTimerLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox actionBox;
        private System.Windows.Forms.CheckBox randomizeCheckBox;
    }
}

