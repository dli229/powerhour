﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public const string defaultTime = "00:00:00";
        int index_ = 0;
        public MainForm()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void playSound(Stream sound)
        {
            try
            {
                System.Media.SoundPlayer snd = new System.Media.SoundPlayer(sound);
                snd.Play();
            }
            catch
            {
                //Don't handle exceptions
            }
        }
        private void startButton_Click(object sender, EventArgs e)
        {
            bool result = parseStrings();
            if (result)
            {
                playSound(Properties.Resources.Mario_Kart_Race_Start___Gaming_Sound_Effect__HD___online_audio_converter_com_);
                Thread.Sleep(5000);
                timerWorker.RunWorkerAsync();
                displayEvent(getAction(), "00:00");
            }
        }

        private void doPrintTimer(string text)
        {
            timerDisplay.Invoke((MethodInvoker)delegate
            {
                timerDisplay.Text = text;
            });
        }
        private void doEventTimer(string text)
        {
            eventTimerLabel.Invoke((MethodInvoker)delegate
            {
                eventTimerLabel.Text = text;
            });
        }

        string getAction()
        {
            if(randomizeCheckBox.Checked)
            { 
                Random random = new Random();
                int index = random.Next(actions_.Count);
                return actions_[index];
            }
            else
            {
                int currentIndex = index_;
                if (currentIndex == actions_.Count - 1)
                {
                    index_ = 0;
                }
                else
                    index_++;

                return actions_[currentIndex];
            }
        }

        private void displayEvent(string text, string timestamp)
        {
            actionBox.Invoke((MethodInvoker)delegate
            {
                actionBox.Text = timestamp + " " + text + System.Environment.NewLine;
            });
        }

        private bool parseStrings()
        {
            try
            {
                using (StreamReader reader = new StreamReader(Properties.Settings.Default.inputFile))
                {
                    string line = reader.ReadLine();
                    while( line != null )
                    {
                        actions_.Add(line);
                        line = reader.ReadLine();
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failed with message:" + ex.Message);
                return false;
            }
        }

        private void doReset()
        {
            index_ = 0;
            doPrintTimer(defaultTime);
            doEventTimer(defaultTime);
            actionBox.Invoke((MethodInvoker)delegate
            {
                actionBox.Clear();
            });
        }

        private void timerWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string formatString = @"hh\:mm\:ss";
            DateTime startTime = DateTime.Now;
            DateTime startEventTime = DateTime.Now;
            DateTime nextTime = startTime;
            DateTime currentTime = startTime;
            String rawTime = totalTimeBox.Text;
            try
            {
                TimeSpan totalTime = TimeSpan.Parse(rawTime);
                TimeSpan eventTime = TimeSpan.Parse(eventTimeBox.Text);
                TimeSpan elapsedTime = nextTime - startTime;
                TimeSpan elapsedEventTime = nextTime - startEventTime;
                TimeSpan eventTimeDiff = eventTime - elapsedEventTime;
                while( elapsedTime <= totalTime )
                {
                    currentTime = DateTime.Now;
                    if(timerWorker.CancellationPending)
                    {
                        doReset();
                        return;
                    }
                    elapsedTime = currentTime - startTime;
                    elapsedEventTime = currentTime - startEventTime;
                    if( elapsedEventTime >= eventTime )
                    {
                        displayEvent(getAction(), elapsedTime.ToString(@"mm\:ss"));
                        startEventTime = currentTime;
                        playSound(Properties.Resources.Mario_Kart_64_Item_Roulette_Sound_Effect__Best_Version_on_the_Internet_So_Far___online_audio_converter_com_);
                    }

                    eventTimeDiff = eventTime - elapsedEventTime;
                    double seconds = Math.Ceiling(eventTimeDiff.TotalSeconds);

                    eventTimeDiff = TimeSpan.FromSeconds(Math.Ceiling(eventTimeDiff.TotalSeconds));

                    doPrintTimer(elapsedTime.ToString(formatString));
                    doEventTimer(eventTimeDiff.ToString(formatString));
                    System.Threading.Thread.Sleep(50);
                }
                
                doPrintTimer(totalTime.ToString(formatString));
                doEventTimer(eventTime.ToString(formatString));
                doReset();
                playSound(Properties.Resources.airhorn_wav__online_audio_converter_com_);

                MessageBox.Show("Done!");
                resetButton_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid time selected: " + ex.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            doReset();
            timerWorker.CancelAsync();
        }

        List<string> actions_ = new List<string>();

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void randomizeCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
